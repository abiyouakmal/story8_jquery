from django.apps import AppConfig


class JqueryAppConfig(AppConfig):
    name = 'jquery_app'
