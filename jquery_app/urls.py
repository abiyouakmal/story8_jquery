from django.urls import path
from . import views

urlpatterns = [
    path('', views.profile_page_view, name='profile_page_app_urls'),
]
