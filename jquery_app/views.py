from django.shortcuts import render


def profile_page_view(request):
    return render(request, 'story8_profile_page.html')
