from django.shortcuts import render
from django.test import TestCase, Client
from django.urls import resolve
from requests import request

from .views import profile_page_view


class Story8(TestCase):
    def test_url_is_exist(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code, 200)

    def test_using_index_func(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, profile_page_view)

    def test_landingpage_containt(self):
        response = self.client.get('')
        html_response = response.content.decode('utf8')
        self.assertIn('Abiyu Muhammad Akmal', html_response)

    def test_story8_formHTML_rendered(self):
        response = self.client.get('/profile/')
        self.assertTemplateUsed(response, 'story8_profile_page.html')
