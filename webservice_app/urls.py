from django.urls import path
from . import views

urlpatterns = [
    path('', views.form_subscriber_view, name='webservice_urls'),
    # untuk bikin url untuk memanggil fungsi validate email di views
    path('validate-email/', views.validate_email, name='validate_email_urls'),
    path('list-subscriber/', views.list_subscriber, name='list_subscriber_urls'),
]
