from django.contrib import admin
from .models import SubscribeModel


class SubscribeAdmin(admin.ModelAdmin):
    pass


admin.site.register(SubscribeModel, SubscribeAdmin)
