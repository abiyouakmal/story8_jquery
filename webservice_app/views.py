from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.shortcuts import render
from django.shortcuts import reverse

from .models import SubscribeModel
from .forms import RegisterSubscriberForm


def form_subscriber_view(request):
    events = SubscribeModel.objects.all().values()

    if request.method == "POST":
        form = RegisterSubscriberForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('webservice_urls'))

    elif request.method == "GET":
        form = RegisterSubscriberForm()

    response = {'form': form, 'events': events}
    return render(request, 'story10_webservice.html', response)


def validate_email(request):
    email = request.GET.get('email')
    data = {
        'is_taken': SubscribeModel.objects.filter(subscriber_email_field=email).exists()
    }
    if data['is_taken']:
        data['error_message'] = 'Your email already taken'

    return JsonResponse(data)

def list_subscriber(request):
    return render(request, 'story10_list_subscriber.html')