from django.db import models


# Create your models here.
class SubscribeModel(models.Model):
    subscriber_name_field = models.TextField(max_length=50)
    subscriber_email_field = models.EmailField(unique=True, error_messages={"unique": "email sudah dipakai !"})
    subscriber_password_field = models.CharField(max_length=10)
