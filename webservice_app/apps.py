from django.apps import AppConfig


class WebserviceAppConfig(AppConfig):
    name = 'webservice_app'
