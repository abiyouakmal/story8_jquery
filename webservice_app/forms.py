from django import forms
from .models import SubscribeModel
from django.forms import ModelForm


class RegisterSubscriberForm(ModelForm):
    class Meta:
        model = SubscribeModel
        fields = ['subscriber_name_field', 'subscriber_email_field', 'subscriber_password_field']

        # Memberikan beberapa widget untuk kolom form
        widgets = {
            'subscriber_name_field': forms.TextInput(attrs={'class': 'form-control ',
                                                            'type': 'text',
                                                            'id': 'id_subscriber_name_field',
                                                            'placeholder': 'Name',
                                                            'background-color': 'black;'}),

            'subscriber_email_field': forms.EmailInput(attrs={'class': 'form-control ',
                                                              'type': 'email',
                                                              'id': 'id_subscriber_email_field',
                                                              'placeholder': 'E-mail',
                                                              }),

            'subscriber_password_field': forms.PasswordInput(attrs={'class': 'form-control ',
                                                                    'type': 'password',
                                                                    'id': 'id_subscriber_password_field',
                                                                    'placeholder': 'Password',
                                                                    }),
        }

        labels = {
            'subscriber_name_field': 'Name',
            'subscriber_email_field': 'E-mail',
            'subscriber_password_field': 'Password',
        }
