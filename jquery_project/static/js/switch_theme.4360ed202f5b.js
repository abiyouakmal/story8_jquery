document.getElementById('blue-button').onclick = switch_theme_to_Blue;
document.getElementById('red-button').onclick = switch_theme_to_Red;

function switch_theme_to_Blue() {
    document.getElementsByTagName('body')[0].style.backgroundColor = '#abbce3';
    document.getElementsByTagName('body')[0].style.color = '#4250d9';
    document.getElementById('navbar_story8')[0].style.backgroundColor = 'blue';
}

function switch_theme_to_Red() {
    document.getElementsByTagName('body')[0].style.backgroundColor = '#e3abab';
    document.getElementsByTagName('body')[0].style.color = '#b32424';
}