var star_active = "https://image.ibb.co/cSJzLL/star-active.png";
var star_deactivate = "https://image.ibb.co/nMfqS0/star-deactivate.png";

function load_book_data(jQuery) {
    console.log("Starting Log..");
    $.ajax({
        url: "/story9-ajax/data-buku-API/",
        dataType: "json",
        success: function (response) {
            console.log("Grabbing info...");
            var entry = "<tbody>";
            response = response['items'];
            for (var i = 0; i < response.length; i++) {
                var title = response[i]["volumeInfo"]["title"];
                var publishInfo = response[i]["volumeInfo"]["publisher"];
                var authors = response[i]["volumeInfo"]["authors"].toString();
                var description = response[i]["volumeInfo"]["description"];
                var cover = response[i]["volumeInfo"]["imageLinks"]["smallThumbnail"];
                // var star = {% %};
                console.log("Success " + i + "!");
                entry += "<tr>" +
                    "<td>" + title + "</td>" +
                    "<td>" + publishInfo + "</td>" +
                    "<td>" + authors + "</td>" +
                    "<td>" + description + "</td>" +
                    "<td><img src='" + cover + "'></td>" +
                    "<td><img id ='" + i + "' src='" + star_deactivate + "' onclick=favorite(" + i + ") ></td>" +
                    "</tr>";
                console.log("Appended " + i);
            }
            // document.getElementById(i).onclick = func;

            entry += "</tbody>";
            console.log("Appending entry...");
            $("#table").append(entry);
        }
    });

}

let count = 0;

function favorite(id) {
    console.log(document.getElementById(id).src);
    console.log(count);
    if (document.getElementById(id).src === star_active) {
        document.getElementById(id).src = star_deactivate;
        count -= 1;
        document.getElementById("counter-favorite-book").innerHTML = "Your Favorite Books: " + count;

    } else if (document.getElementById(id).src === star_deactivate) { // jika === dia ngecek data typenya
        document.getElementById(id).src = star_active;
        count += 1;
        document.getElementById("counter-favorite-book").innerHTML = "Your Favorite Books: " + count;


    }
}

$(document).ready(load_book_data());
