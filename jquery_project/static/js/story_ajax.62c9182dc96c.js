var star_active = "https://image.ibb.co/cSJzLL/star-active.png";
var star_deactivate = "https://image.ibb.co/nMfqS0/star-deactivate.png";

function load_book_data(jQuery) {
    console.log("Starting Log..");
    $.ajax({
        /* Mengambil data dary json yang sudah saya get
         untuk menjadi bagian dari website saya
         sehingga saya getnya menggunakan url saya*/
        url: "/story9-ajax/data-buku-API/",
        dataType: "json",
        success: function (response) {
            /* Pada console.log mempunyai tujuan untuk
             memunculkan sesuatu di dalam console browser yang kita miliki */
            console.log("Grabbing info...");

            /* Disini bagian untuk mengambil data dari json dan menuliskan sesuatu pada
            kodingan yang akan membentuk table tersebut*/
            var entry = "<tbody>";
            response = response['items'];
            // iterasi untuk pengambilan informasi
            for (var i = 0; i < response.length; i++) {
                var title = response[i]["volumeInfo"]["title"];
                var publishInfo = response[i]["volumeInfo"]["publisher"];
                var authors = response[i]["volumeInfo"]["authors"].toString();
                var description = response[i]["volumeInfo"]["description"];
                var cover = response[i]["volumeInfo"]["imageLinks"]["smallThumbnail"];

                // iterasi saat peulisan kode untuk memuat tablenya
                console.log("Success " + i + "!");
                entry += "<tr>" +
                    "<td>" + title + "</td>" +
                    "<td>" + publishInfo + "</td>" +
                    "<td>" + authors + "</td>" +
                    "<td>" + description + "</td>" +
                    "<td><img src='" + cover + "'></td>" +
                    "<td><img id ='" + i + "' src='" + star_deactivate + "' onclick=favorite(" + i + ") ></td>" +
                    "</tr>";
                console.log("Appended " + i);
            }
            entry += "</tbody>";

            // cara untuk memngoper nilai tersebut ke dalam tabel-tabel tersebut
            console.log("Appending entry...");
            $("#table").append(entry);
        }
    });

}

let count = 0;
function favorite(id) {
    // untuk cek saja pada console
    console.log(document.getElementById(id).src);
    console.log(count);

    // jika bintang dalam posisi aktif (active)
    if (document.getElementById(id).src === star_active) {
        //proses pengubahan star dari bernyala nyala jadi gelap
        document.getElementById(id).src = star_deactivate;
        count -= 1;
        document.getElementById("counter-favorite-book").innerHTML = "Your Favorite Books: " + count;

    // jika bintang dalam posisi mati (deactive)
    } else if (document.getElementById(id).src === star_deactivate) { // jika === dia ngecek data typenya
        //proses pengubahan bintang dari gelap menjadi bernyala-nyala
        document.getElementById(id).src = star_active;
        count += 1;
        document.getElementById("counter-favorite-book").innerHTML = "Your Favorite Books: " + count;


    }
}

$(document).ready(load_book_data());
