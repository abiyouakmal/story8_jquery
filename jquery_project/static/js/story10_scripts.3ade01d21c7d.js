// Variabel global
var username_taken = false;


var func = function () {
    if ($('#id_subscriber_email_field').val() !== "") {
        console.log("Masuk sini")
        $.ajax({
            // mengatur si url di
            url: '/story10-webservice/validate-email/', // manggil fungsi di views.py melalui urls dengan ngeset link nya
            // saya harus melempar nilai key 'email_views_variable' biar bisa diakses nilainya
            data: {email_views_variable: $('#id_subscriber_email_field').val()},
            success: function (response) { // response ini adalah return Json dari fungsi validate-email di views.py
                console.log(response);
                if ($("#id_subscriber_name_field").val() !== "" && $("#id_subscriber_email_field").val() !== "" && $('#id_subscriber_password_field').val() !== "") {
                    if (response.is_taken) {
                        // email_already_taken_func - JIKA TIDAK DI AJAX MAKA VARIABLE TIDAK TERUPDATE REALTIME
                        // HARUS DI TARUH DI AJAX KARENA FUNGSI INI PAS AJAX ITU SUCESS DILAKUKAN
                        email_already_taken_func(true);
                        if (!document.getElementById('id_email_already_taken_warning')) {
                            $("#warning_div").append("<a id='id_email_already_taken_warning' style='color: #ffffff;'>your email already taken!</a>");
                        }
                    } else if (!response.is_taken) {
                        if (document.getElementById('id_email_already_taken_warning')) {
                            $('#id_email_already_taken_warning').remove();
                        }
                        email_already_taken_func(false);
                    }
                } else {
                    $('#submit-button').prop('disabled', true);
                }
            }
        })
    }


    // language=JQuery-CSS
    function email_already_taken_func(email_taken) {
        console.log("tahap satu")
        if ($("#id_subscriber_name_field").val() !== "" && $("#id_subscriber_email_field").val() !== "" && $('#id_subscriber_password_field').val() !== "") {
            console.log("tahap dua")
            if (email_taken === false) {
                // Tujuannya untuk mematikan mengaktifkan buttonnya
                $('#submit-button').removeAttr('disabled')
            } else if (email_taken === true) {
                // tujuannya untuk mengaktifkan kembali attribut disabled
                $('#submit-button').prop('disabled', true)
            }
        }
    }


}


function check_success_submit() {
    console.log("=== SUBMITTED ===");
    var submit_success_celebrate = ("<p style = 'color : #cae9fa;'>Congratulation! You are officially become subscriber!</p>");
    $("#congratulation_greeting").append(submit_success_celebrate);
    alert("Congratulation! You are officially become subscriber!")
}

var delayInMilliseconds = 10000; //2 second
setTimeout(function () {
    //your code to be executed after 2   second
}, delayInMilliseconds)


// menjalankan JS saat semua dokumen pada web page sudah terload
$(document).ready(function () {
    $('#id_subscriber_name_field').keyup(func);
    $('#id_subscriber_email_field').keyup(func);
    $('#id_subscriber_password_field').keyup(func);
})
