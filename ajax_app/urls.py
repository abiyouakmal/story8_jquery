from django.urls import path
from . import views

urlpatterns = [
    path('', views.story_9_page_view, name='story9_ajax'),
    path('login/', views.login_page, name='views_login_page'),
    path('login-success/', views.login_success, name='views_login_success'),
    path('logged-out/', views.logout_view, name='views_logout'),
    # dibagian ini yang berfungsi untuk mendapatkan json nya
    path('<slug:num>/', views.story_9_booklist, name='views_story9_book_data'),
]
