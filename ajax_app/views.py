from django.shortcuts import render, redirect
import urllib.request
import json
from django.http import JsonResponse
from django.contrib.auth.models import User
from django.contrib.auth import logout, login


def story_9_page_view(request):
    return render(request, 'story9_ajax.html')


def story_9_booklist(request, num):
    url = 'https://www.googleapis.com/books/v1/volumes?q=' + num
    print(url)
    req = urllib.request.Request(url)

    # parsing response
    r = urllib.request.urlopen(req).read()
    cont = json.loads(r.decode('utf-8'))
    return JsonResponse(cont)


def login_page(request):
    return render(request, 'login.html')


def login_success(request):
    return render(request, 'login_page.html')


def logout_view(request):
    request.session.flush()
    logout(request)
    return redirect("/story9-ajax/")
